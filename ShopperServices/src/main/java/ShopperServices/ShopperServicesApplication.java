package ShopperServices;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ShopperServicesApplication {

    public static void main(String[] args) {
        SpringApplication.run(ShopperServicesApplication.class, args);
    }
}
