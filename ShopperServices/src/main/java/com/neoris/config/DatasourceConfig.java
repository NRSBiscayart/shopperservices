package com.neoris.config;

import javax.naming.NamingException;
import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jndi.JndiTemplate;

@Configuration
public class DatasourceConfig {
	
    @Bean
    public DataSource dataSource() throws NamingException {
    	
        JndiTemplate jndi = new JndiTemplate();
        DataSource dataSource = (DataSource) jndi.lookup("java:jdbc/SHOPPER_DB");

        return dataSource;
    }

}
