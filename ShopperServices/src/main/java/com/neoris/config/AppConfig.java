package com.neoris.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration 
@ComponentScan("com.neoris") 
@EnableWebMvc 
public class AppConfig extends WebMvcConfigurerAdapter {

}
