/**
 * 
 */
package com.neoris.templates;

/**
 * @author alejandro.biscayart
 *
 */
public class BeaconInformation {
	
	private int id;
	private String beaconType;
	private String beaconOwner;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getBeaconType() {
		return beaconType;
	}
	public void setBeaconType(String beaconType) {
		this.beaconType = beaconType;
	}
	public String getBeaconOwner() {
		return beaconOwner;
	}
	public void setBeaconOwner(String beaconOwner) {
		this.beaconOwner = beaconOwner;
	}
	
	
}
