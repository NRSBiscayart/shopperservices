/**
 * 
 */
package com.neoris.entities;

/**
 * @author alejandro.biscayart
 *
 */
public class EntityType {

	private int entityTypeId;
	private String entityTypeDescription;
	
	public int getEntityTypeId() {
		return entityTypeId;
	}
	public void setEntityTypeId(int entityTypeId) {
		this.entityTypeId = entityTypeId;
	}
	public String getEntityTypeDescription() {
		return entityTypeDescription;
	}
	public void setEntityTypeDescription(String entityTypeDescription) {
		this.entityTypeDescription = entityTypeDescription;
	}
	
	
}
