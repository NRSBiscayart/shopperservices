/**
 * 
 */
package com.neoris.entities;

/**
 * @author alejandro.biscayart
 *
 */
public class Beacon {
	
	private int uuid;
	private Entity entity;
	
	public int getUuid() {
		return uuid;
	}
	public void setUuid(int uuid) {
		this.uuid = uuid;
	}
	public Entity getEntity() {
		return entity;
	}
	public void setEntity(Entity entity) {
		this.entity = entity;
	}
	

}
