/**
 * 
 */
package com.neoris.businesslogic;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.neoris.dataaccess.BeaconDao;
import com.neoris.entities.Beacon;
import com.neoris.templates.BeaconInformation;

/**
 * @author alejandro.biscayart
 *
 */
@Component
public class BeaconBL {
	
	@Autowired
	private BeaconDao beaconDao;
	
	
	public BeaconInformation getBeacon(int beaconId){
		
		Beacon beacon = beaconDao.getBeacon(beaconId);
		
		BeaconInformation beaconInformation = new BeaconInformation();
		String entityKey = beacon.getEntity().getEntityKey();
		
		try{
			beaconInformation.setId(Integer.parseInt(entityKey));
		} catch(NumberFormatException nfe){
			beaconInformation.setId(0);
		}
		
		beaconInformation.setBeaconType(beacon.getEntity().getEntityType().getEntityTypeDescription());
		beaconInformation.setBeaconOwner(beacon.getEntity().getEntityCategory());
		
		return beaconInformation;
	}
	

	public BeaconDao getBeaconDao() {
		return beaconDao;
	}

	public void setBeaconDao(BeaconDao beaconDao) {
		this.beaconDao = beaconDao;
	}
	
	
	

}
