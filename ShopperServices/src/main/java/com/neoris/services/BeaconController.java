/**
 * 
 */
package com.neoris.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.neoris.businesslogic.BeaconBL;
import com.neoris.templates.BeaconInformation;

/**
 * @author alejandro.biscayart
 *
 */
public class BeaconController {
	
	@Autowired
	private BeaconBL beaconBL;
	
	

	@RequestMapping("/BeaconInformation/{beaconId}")
	public BeaconInformation getBeaconInformation(@RequestParam(value="beaconId") int beaconId) {
		//TODO Define the parameter name
	        return beaconBL.getBeacon(beaconId);
	}
	
	
	
	public BeaconBL getBeaconBL() {
		return beaconBL;
	}

	public void setBeaconBL(BeaconBL beaconBL) {
		this.beaconBL = beaconBL;
	}

}
